const fsP = require('fs/promises')
const express = require('express')
const bodyParser = require('body-parser')
const sqlite3 = require('sqlite3').verbose()
const mustache = require('mustache')

const port = process.env.PORT || 33000
const app = express()
const dbPath = process.env.DB_PATH || './blah.db'
const db = new sqlite3.Database(dbPath)
const log = {
  info: console.info,
  warn: console.warn,
  error: console.error,
}

// FIXME add litestream
// FIXME add CD to deploy to GCP Run with 1 replica

app.get('/', async (req, res) => {
  const rows = await getData()
  return render(res, 'index', {theDate: Date.now(), rows}, ['singlerow'])
})

app.post('/item', bodyParser.json(), (req, res) => {
  const { val } = req.body
  if (!val) {
    // FIXME render HTML
    return res.status(400).send({msg: 'no val supplied'})
  }
  log.info(`Inserting value: ${val}`)
  try {
    db.run('INSERT INTO todos VALUES (?)', val, function (err) {
      if (err) {
        throw new Error('FIXME render an error template')
      }
      // FIXME should we actually read the data back from the DB?
      return render(res, 'singlerow', {id: this.lastID, title: val})
    })
  } catch (err) {
    log.error('Failed to insert value', err)
    // FIXME render error HTML
    return res.status(500).send({populationFailTown: 'you'})
  }
})

app.delete('/item/:id', (req, res) => {
  const { id } = req.params
  if (!id) {
    // FIXME render HTML
    return res.status(400).send({msg: 'no id supplied'})
  }
  log.info(`Deleting ID: ${id}`)
  try {
    db.run('DELETE FROM todos WHERE rowid = ?', id, function (err) {
      if (err) {
        throw new Error('FIXME render an error template')
      }
      return res.set('Content-Type', 'text/html').send('')
    })
  } catch (err) {
    log.error(`Failed to delete item with id=${id}`, err)
    // FIXME render error HTML
    return res.status(500).send({populationFailTown: 'you'})
  }
})

app.get('/htmx.js', async (req, res) => {
  const htmxJs = await readTextFile('./node_modules/htmx.org/dist/htmx.min.js')
  const jsonEncJs = await readTextFile('./node_modules/htmx.org/dist/ext/json-enc.js')
  const body = htmxJs + jsonEncJs
  return res
    .set('Content-Type', 'text/javascript')
    .send(body)
})

app.get('/styles.css', async (req, res) => {
  const body = await readTextFile('./styles.css')
  return res
    .set('Content-Type', 'text/css')
    .send(body)
})

;(async () => {
  await new Promise(r => {
    db.serialize(() => {
      db.get("SELECT count(*) AS c FROM todos", (err, resultRow) => {
        if (!err) {
          log.info(`DB and table exists, ${resultRow.c} rows, nothing to do`)
          return r()
        }
        log.warn('DB (probably) does not exist, creating it. The error:', err)
        db.run("CREATE TABLE todos (title TEXT)")
        return r()
        // FIXME do we need db.close() on shutdown?
      })
    })
  })
  app.listen(port, () => {
    log.info(`listening on port ${port}`)
  })
})()

async function getData() {
  const rows = await new Promise((resolve, reject) => {
    db.all('SELECT rowid AS id, title FROM todos', (err, result) => {
      if (err) {
        return reject(new Error('Failed to get rows', err))
      }
      log.info(`Found ${result.length} rows`)
      return resolve(result)
    })
  })
  return rows
}

function readTextFile(thePath) {
  return fsP.readFile(thePath, 'utf-8')
}

function readView(viewName) {
  return readTextFile(`./views/${viewName}.html`)
}

async function render(res, templateName, view, partials) {
  const template = await readView(templateName)
  const partialsMapping = await (async () => {
    if (!partials) {
      return
    }
    const result = {}
    // FIXME should probably do in parallel, and cache result
    for (const curr of partials) {
      result[curr] = await readView(curr)
    }
    return result
  })()
  return res.send(mustache.render(template, view, partialsMapping))
}
